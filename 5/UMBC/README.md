# TrojiAI - UMBC - Round 5
This repo contains codes for the round 5 implementation of UMBC for TrojiAI project.

If you would like to refer to it, please contact oates@cs.umbc.edu

## Getting Started
These instructions will get you running the codes.

### Requirements
* Python 3.7 or higher
* Pytorch >= 1.3
* Pytorch_transformers
* Numpy, Pickle

### Running the detector
Please run `./models/detector.py` to evaluate whether a model is trojaned or not. This script accepts the following arguments:

-   `--model_filepath`: the file path to the binary model file to be evaluated.
-   `--cls_token_is_first`: whether the cls token is at the beginning of the sequence.
-   `--tokenizer_filepath`: the file path to the binary tokenizer file used by the model.
-   `--embedding_filepath`: the file path to the embedding model used by the model.
-   `--result_filepath`: the file path to write the output to.
-   `--scratch_dirpath`: the folder path to temporary stored files when doing the evaluation.
-   `--examples_dirpath`: the folder path of possible examples can be used.
-   `--seed`: the random seed to be used.
-   `--detector`: the file path to the trained detector model.
-   `--n_jacobian_per_sample`: the number of jacobian matrices generated for each example used.
-   `--n_bootstraps`: the number of bootstraps.

### Training the detector
Please run `./models/train.py` to train the detector. This script accepts the following arguments:

-  `--round_root_dir`: the folder path of the round dataset.
-  `--model_root_dir`: the folder path of the models in the round dataset.
-  `--output_dir`: the output directory of trained detectors.
-  `--n_jacobian_per_sample`: the number of jacobian matrices generated for each example used.
-  `--n_bootstraps`: the number of bootstraps.
-  `--dev_set_size`: the number of dev examples.
-  `--learning_rate`: the learning rate.
-  `--weight_decay`: the weight decay.
-  `--training_size`: the number of training examples.
-  `--num_epochs`: the number of epochs.
-  `--batch_size`: the batch size to use.
-  `--num_warmup_steps`: the number of warmup steps.
-  `--seed`: the random seed to be used. 
