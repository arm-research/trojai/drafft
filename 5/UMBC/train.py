import argparse
import collections
import json
import os
import random
import sys
import time
from functools import partial

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import AdamW
from torch.optim.lr_scheduler import LambdaLR

import numpy as np
from tqdm import tqdm
import trojai
from sklearn.metrics import roc_auc_score


class ModelHandler(object):

    def __init__(self, model_root_dir):
        super(ModelHandler, self).__init__()
        self.model_root_dir = model_root_dir

    def get_model_by_id(self, model_id, device=None):
        model = torch.load(os.path.join(self.model_root_dir, model_id, "model.pt"))
        model.zero_grad()
        if device is not None:
            model.to(device)
        return model

    def get_label_by_ids(self, model_ids, device=None):
        labels = []
        for model_id in model_ids:
            config = self.get_model_config(model_id)
            label = 1 if config["poisoned"] else 0
            labels.append(label)
        labels = torch.FloatTensor(labels)
        if device is not None:
            labels = labels.to(device)
        return labels

    def get_model_by_ids(self, model_ids, device=None):
        return [self.get_model_by_id(model_id, device) for model_id in model_ids]

    def get_model_config(self, model_id):
        config_filepath = os.path.join(self.model_root_dir, model_id)
        with open(os.path.join(config_filepath, "config.json"), "r") as fp:
            config = json.load(fp)
        return config

    def free(self, models):
        for model in models:
            model.cpu()


class UltimateDetector(nn.Module):
    """ A model detector

    Params:
    input_size:     the input size of query samples
    """
    def __init__(self, input_size):
        super(UltimateDetector, self).__init__()
        self.input_size = input_size

        self.norm_layer = nn.LayerNorm((input_size * 6,))
        self.cls = nn.Sequential(
                        nn.Dropout(0.5),
                        nn.Linear(input_size * 6, input_size * 3),
                        nn.GELU(),
                        nn.Dropout(0.5),
                        nn.Linear(input_size * 3, input_size),
                        nn.GELU(),
                        nn.Dropout(0.5),
                        nn.Linear(input_size, 1),
                        nn.Sigmoid())

    def forward(self, x, n_bootstraps=100):
        # x (batch_size, n_samples, *)
        # bootstrapped_x (batch_size, n_bootstraps, *)
        bootstrapped_x = bootstrap_sample(x, n_bootstraps=n_bootstraps)
        batch_size, _, hidden_size = bootstrapped_x.size()
        bootstrapped_x = bootstrapped_x.view(batch_size * n_bootstraps, hidden_size)
        bootstrapped_x = self.norm_layer(bootstrapped_x)
        logits = self.cls(bootstrapped_x)
        logits = logits.view(batch_size, n_bootstraps)
        return torch.median(logits, dim=1)[0]


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)


def get_linear_schedule_with_warmup(optimizer, num_warmup_steps, num_training_steps, last_epoch=-1):
    """ Create a schedule with a learning rate that decreases linearly after
    linearly increasing during a warmup period.
    """

    def lr_lambda(current_step):
        if current_step < num_warmup_steps:
            return float(current_step) / float(max(1, num_warmup_steps))
        return max(
            0.0, float(num_training_steps - current_step) / float(max(1, num_training_steps - num_warmup_steps))
        )

    return LambdaLR(optimizer, lr_lambda, last_epoch)


def load_dataset(dataset, handler, n_samples, device, mean=0., var=1.):
    """Generate data for each model provided"""

    # randomly shuffle the dataset
    np.random.shuffle(dataset)

    # obtain the meta info and label for each model
    X, Y = [], []
    for model_id in tqdm(dataset, ascii=True):
        # obtain the label
        config = handler.get_model_config(model_id)
        label = 1 if config["poisoned"] else 0

        # obtain the model
        model = handler.get_model_by_id(model_id)
        model.to(device)

        # obtain the meta info
        sample = torch.randn(n_samples, 768) * var + mean
        sample = sample.to(device)

        model_x = generate_jacobian_for_model(model, sample)
        model_x = model_x.detach().cpu()
        model_x = torch.flatten(model_x, -2, -1)

        X.append(model_x)
        Y.append(label)

    return X, Y


def generate_jacobian_for_model(model, sample):
    """Generate Jacobian matrices for each model"""
    sample = sample.unsqueeze(1)
    with torch.backends.cudnn.flags(enabled=False):
        jacobian = torch.autograd.functional.jacobian(lambda x: model(x), sample)
        result = [jacobian[i, :, i] for i in range(sample.size(0))]
        result = torch.stack(result, dim=0)
        result = result.squeeze(-2)
    return result


def bootstrap_sample(batched_jacobians, n_bootstraps=100):
    # batched_jacobians:   (batch_size, n_jacobian_per_sample, *)
    max_index = batched_jacobians.size(1)

    bootstraps = []
    for i in range(n_bootstraps):
        length = random.randint(5, max_index - 1)
        indices = torch.randint(0, max_index, (length,), device=batched_jacobians.device)
        jacobians = torch.index_select(batched_jacobians, 1, indices)
        jacobian_mean = torch.mean(jacobians, dim=1)
        jacobian_max, _ = torch.max(jacobians, dim=1)
        jacobian_min, _ = torch.min(jacobians, dim=1)
        jacobian = torch.cat([jacobian_mean, jacobian_max, jacobian_min], dim=-1)
        bootstraps.append(jacobian)
    return torch.stack(bootstraps, dim=1)


def evaluate(model, dataset, device, batch_size=25, n_bootstraps=100):
    data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=False)

    cross_entropy_loss, total_samples = 0., 0
    preds, golds = [], []
    for batch in data_loader:
        x, y = batch
        x, y = x.to(device), y.to(device)

        with torch.no_grad():
            model.eval()
            logits = model(x, n_bootstraps=n_bootstraps)
            loss_fn = nn.BCELoss(reduction="sum")
            cross_entropy_loss += loss_fn(logits, y).item()
            total_samples += x.size(0)

            preds.append(logits.detach().cpu())
            golds.append(y.detach().cpu())

    preds = torch.cat(preds, dim=0)
    golds = torch.cat(golds, dim=0)
    preds, golds = preds.numpy(), golds.numpy()
    score = roc_auc_score(golds, preds)

    cross_entropy_loss /= total_samples
    return cross_entropy_loss, {"auc_score": score, "cross_entropy_loss": cross_entropy_loss}


def train(args):
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    handler = ModelHandler(args.model_root_dir)
    if torch.cuda.is_available():
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")

    # get all the models
    all_model_ids = os.listdir(args.model_root_dir)

    # split the dataset
    np.random.shuffle(all_model_ids)
    dev_set = all_model_ids[: args.dev_set_size]
    train_set = all_model_ids[args.dev_set_size:]

    # convert datasets into tensors
    print("Generating the data for each model...")
    train_X, train_Y = load_dataset(train_set, handler, args.n_jacobian_per_sample, device)

    dev_X, dev_Y = load_dataset(dev_set, handler, args.n_jacobian_per_sample, device)
    dev_X, dev_Y = torch.stack(dev_X, dim=0), torch.FloatTensor(dev_Y)
    dev_data = TensorDataset(dev_X, dev_Y)

    # initialize the model
    model = UltimateDetector(768)
    model.to(device)

    # initialize the optimizer and scheduler
    n_steps_per_epoch = (len(train_X) + args.batch_size - 1) // args.batch_size
    t_total = n_steps_per_epoch * args.num_epochs
    no_decay = ["bias", "LayerNorm.weight"]
    optimizer = AdamW([
        {"params": [p for n, p in model.named_parameters() if not any(nd in n for nd in no_decay)],
            "lr": args.learning_rate, "weight_decay": args.weight_decay},
        {"params": [p for n, p in model.named_parameters() if any(nd in n for nd in no_decay)],
            "lr": args.learning_rate, "weight_decay": 0.0}
    ])
    scheduler = get_linear_schedule_with_warmup(
        optimizer, num_warmup_steps=args.num_warmup_steps, num_training_steps=t_total
    )

    # start training
    best_score = float('inf')
    for epoch in range(args.num_epochs):
        total_loss, local_steps = 0., 0

        # epoch_X, epoch_Y = over_sampling_balanced_data(train_X, train_Y)
        epoch_X = torch.stack(train_X, dim=0)
        epoch_Y = torch.FloatTensor(train_Y)
        train_data = TensorDataset(epoch_X, epoch_Y)
        train_loader = DataLoader(train_data, shuffle=True, batch_size=args.batch_size)

        for batch in train_loader:
            x, y = batch
            x, y = x.to(device), y.to(device)

            model.train()
            optimizer.zero_grad()

            logits = model(x, n_bootstraps=args.n_bootstraps)
            loss_fn = nn.BCELoss()
            loss = loss_fn(logits, y)

            loss.backward()
            total_loss += loss.item()
            local_steps += 1

            optimizer.step()
            scheduler.step()

        total_loss /= local_steps
        score, all_scores = evaluate(model, dev_data, device, batch_size=args.batch_size, n_bootstraps=args.n_bootstraps)
        print("Epoch: %d, dev score: %f, avg loss: %f" % (epoch + 1, score, total_loss))
        if score < best_score:
            for key, val in all_scores.items():
                print("%s: %f" % (key, val))
            print("Saving the best model")
            best_score = score
            torch.save(model.state_dict(), os.path.join(args.output_dir, "%d.pt" % args.seed))


def parse_args():
    parser = argparse.ArgumentParser(description="TrojiAI detector")
    parser.add_argument("--round_root_dir",
                        type=str,
                        help="Root dir of the round5 data.",
                        default="../round5-train-dataset")
    parser.add_argument("--model_root_dir",
                        type=str,
                        help="Root dir of the round5 model data",
                        default="../round5-train-dataset/models")
    parser.add_argument("--output_dir",
                        type=str,
                        help="The output dir of the trained models.",
                        default="./models")
    parser.add_argument("--n_jacobian_per_sample",
                        type=int,
                        help="The number of query samples to use.",
                        default=100)
    parser.add_argument("--n_bootstraps",
                        type=int,
                        help="The number of bootstrapped examples to use",
                        default=10)
    parser.add_argument("--dev_set_size",
                        type=int,
                        help="The number of dev set examples.",
                        default=50)
    parser.add_argument("--learning_rate",
                        type=float,
                        help="The learning rate to train the model.",
                        default=5e-4)
    parser.add_argument("--weight_decay",
                        type=float,
                        help="The weight for L2 regularization",
                        default=0.)
    parser.add_argument("--training_size",
                        type=int,
                        help="The number of training examples to train the detector.",
                        default=1000)
    parser.add_argument("--num_epochs",
                        type=int,
                        help="Number of total epochs to run",
                        default=100)
    parser.add_argument("--batch_size",
                        type=int,
                        help="Train batch size for labeled data.",
                        default=25)
    parser.add_argument("--num_warmup_steps",
                        type=int,
                        help="The number of warm up steps",
                        default=0)
    parser.add_argument("--seed",
                        type=int,
                        help="The random seed",
                        default=1234)

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    set_seed(args.seed)
    train(args)
