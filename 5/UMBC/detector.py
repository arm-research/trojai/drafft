import argparse
import collections
import json
import os
import random
import sys
import time
from functools import partial

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import TensorDataset, DataLoader
from torch.optim import AdamW
from torch.optim.lr_scheduler import LambdaLR

import numpy as np
from tqdm import tqdm
import trojai
from sklearn.metrics import roc_auc_score

from train import generate_jacobian_for_model, UltimateDetector


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)


def predict_for_victim(victim,
                       model,
                       n_jacobian_per_sample,
                       n_bootstraps,
                       device,
                       mean=0.,
                       var=1.):
    # generate normal distribution samples
    sample = torch.randn(n_jacobian_per_sample, 768) * var + mean
    sample = sample.to(device)

    model_x = generate_jacobian_for_model(victim, sample)
    model_x = model_x.view(1, n_jacobian_per_sample, -1)
    model_x = model_x.to(device)

    with torch.no_grad():
        model.to(device)
        model.eval()
        logits = model(model_x, n_bootstraps=n_bootstraps)
        return logits.detach().cpu()


def main(args):
    # set up the constants
    if torch.cuda.is_available():
        device = torch.device("cuda:0")
    else:
        device = torch.device("cpu")

    # load all the models
    all_state_dicts = torch.load(args.detector)
    model_forest = []
    for state_dict in all_state_dicts:
        model = UltimateDetector(768)
        model.load_state_dict(state_dict)
        model_forest.append(model)

    # load the victim model
    victim = torch.load(args.model_filepath)
    victim.to(device)

    # start predicting
    predictions = []
    for model in model_forest:
        logits = predict_for_victim(victim, model, args.n_jacobian_per_sample, args.n_bootstraps, device)
        predictions.append(logits.view(-1))
    predictions = torch.cat(predictions, dim=0)
    prediction = torch.median(predictions).item()

    print(prediction)
    with open(args.result_filepath, "w") as fp:
        fp.write("{}".format(prediction))



def parse_args():
    parser = argparse.ArgumentParser(description="TrojiAI detector")

    parser.add_argument('--model_filepath',
                        type=str,
                        help='File path to the pytorch model file to be evaluated.',
                        default='./model/model.pt')
    parser.add_argument('--cls_token_is_first',
                        help='Whether the first embedding token should be used as the summary of the text sequence, or the last token.',
                        action='store_true',
                        default=False)
    parser.add_argument('--tokenizer_filepath',
                        type=str,
                        help='File path to the pytorch model (.pt) file containing the correct tokenizer to be used with the model_filepath.',
                        default='./model/tokenizer.pt')
    parser.add_argument('--embedding_filepath',
                        type=str,
                        help='File path to the pytorch model (.pt) file containing the correct embedding to be used with the model_filepath.',
                        default='./model/embedding.pt')
    parser.add_argument('--result_filepath',
                        type=str,
                        help='File path to the file where output result should be written. After execution this file should contain a single line with a single floating point trojan probability.',
                        default='./output.txt')
    parser.add_argument('--scratch_dirpath',
                        type=str,
                        help='File path to the folder where scratch disk space exists. This folder will be empty at execution start and will be deleted at completion of execution.',
                        default='./scratch')
    parser.add_argument('--examples_dirpath',
                        type=str,
                        help='File path to the folder of examples which might be useful for determining whether a model is poisoned.',
                        default='./model/clean_example_data')
    parser.add_argument("--seed",
                        type=int,
                        help="The random seed",
                        default=1234)

    parser.add_argument("--detector",
                        type=str,
                        help="the path of the trained detector models.",
                        default="/all_state_dicts.pt")
    parser.add_argument("--n_jacobian_per_sample",
                        type=int,
                        help="The number of examples to generate for each model.",
                        default=100)
    parser.add_argument("--n_bootstraps",
                        type=int,
                        help="The number of bootstrapped examples to use",
                        default=10)
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    set_seed(args.seed)
    main(args)
